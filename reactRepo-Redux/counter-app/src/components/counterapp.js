import React, {Component} from 'react';
import Increment from './components/increment';
import Decrement from './components/decrement';
import Result from './components/result';
import {connect} from 'react-redux';

class CounterApp extends Component {
    constructor(props){
        super(props);

    }
    render (){
        return (
            <div className="container">
                <Increment />
                <Result />
                <Decrement />
            </div>
        )
    }
}
export default connect();