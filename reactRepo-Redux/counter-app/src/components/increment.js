import React from 'react';

const Increment = (props)=> {
    return (
        <button 
            className="increment btn- btn-primary" 
            onClick={props.increment}> </button>
    );
}

export default Increment;