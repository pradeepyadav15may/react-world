const initialState = {
    searchText: "Start Typing",
    filteredItems: [],
}

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case 'SEARCHITEM':
            const filteredItems = itemDetails.filter((item) => {
                return item.itemType.includes(action.payload);
            });

            return {
                filteredItems: filteredItems
            }
        default:
        return state;
    }
}

const itemDetails = [
    {
        itemName: "Radio",
        itemType: "electronics"
    },
    {
        itemName: "TV",
        itemType: "electronics"
    },
    {
        itemName: "Car",
        itemType: "vehicle"
    },
    {
        itemName: "Pen",
        itemType: "stationery"
    },
    {
        itemName: "mobile",
        itemType: "electronics"
    },
    {
        itemName: "jeans",
        itemType: "cloth"
    },
    {
        itemName: "t-shirt",
        itemType: "cloth"
    }
]
export default reducer;

//#TM: Looks ok. 