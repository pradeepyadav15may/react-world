import React from 'react';

const ItemsList = (props) => {
    console.log(props);
        const itemsList = props.filteredItems.map((filteredItem) => {
            
            return (
                <li>
                    {filteredItem.itemName}
                </li>
            )
        });

        return (
            <div className="itemsList">
                <ul className="searchItemsList">
                    {itemsList}
                </ul>
            </div>
        )
    };

export default ItemsList;
