import React, { Component } from 'react';
import ItemsListContainer from '../containers/itemsListContainer.js';

class AutoCompleteApp extends Component {
    render() {
        return (
            <div className="autocomplete-app-wrapper">
                <input type="text" className="inputText" onChange={this.props.searchItem} placeholder={this.props.searchText}/>
                <ItemsListContainer />
            </div>
        )
    }
}

export default AutoCompleteApp;