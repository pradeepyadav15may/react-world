import {connect} from 'react-redux';
import ItemsList from '../components/itemslist.js';

const initialState = {
    filteredItems: [],
}
const mapStateToProps = (state = initialState) => ({
    filteredItems: state.filteredItems,
});
const ItemsListContainer = connect(mapStateToProps)(ItemsList);
export default ItemsListContainer;