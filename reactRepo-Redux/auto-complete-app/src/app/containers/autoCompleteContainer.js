import {connect} from 'react-redux';
import AutoCompleteApp from '../components/autocompleteapp.js';

const initialState = {
    searchText: "",
}
const mapStateToProps = (state = initialState) => ({
    searchText: state.searchText,
});

const mapDispatchToProps = (dispatch) => {
    return {
        searchItem: (e) => dispatch({
            type: "SEARCHITEM",
            payload: e.target.value
        })
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(AutoCompleteApp);