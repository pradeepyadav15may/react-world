import {createStore} from 'redux';
import reducer from '../reducers/autocompletereducer.js'

const store = createStore(reducer);

export default store;