const initialState = {
    myPost: "",
    location: "",
    placeholder: "Kya Chal Raha hai?",
    postText: "Post Kar Lo",
    locationLinkText: "Ho Kahan Par?",
    postsList: [],
}

const socialMediaReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'POSTITBTNCLICK':
            return {
                ...state,
                postsList : [...state.postsList, {
                    postValue: action.payload,
                    location: ""
                }]
            }
        default:
        return state;
    }
};

export default socialMediaReducer;