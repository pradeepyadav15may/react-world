import {connect} from 'react-redux';
import MyPostComponent from '../components/myPostComponent.js';

const mapStateToProps = (state) => ({
        myPost: state.myPost,
        location: state.location,
        placeholder: state.placeholder,
        postText: state.postText,
        locationLinkText: state.locationLinkText,
    }
);

const mapDispatchToProps = (dispatch) => {
    return {
        postItBtnClick: (e) => dispatch({
            type: "POSTITBTNCLICK",
            payload: document.getElementById("inputTextID").value,
        })
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(MyPostComponent);
