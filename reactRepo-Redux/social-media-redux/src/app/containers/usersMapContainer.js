import React, {Component} from 'react';
import {Map, InfoWindow, Marker, GoogleApiWrapper} from 'google-maps-react';
 
export class MapContainer extends Component {
    render() {
        const style = {
            width: '100%',
            height: '100%'
          };
      return (
        <Map google={this.props.google} 
            zoom={14}
            style={style}
            >
   
          <Marker onClick={this.onMarkerClick}
                  name={'Current location'} />
   
          <InfoWindow onClose={this.onInfoWindowClose}>
              <div>
                <h1>{this.state.selectedPlace.name}</h1>
              </div>
          </InfoWindow>
        </Map>
      );
    }
  }
 
export default GoogleApiWrapper({
  apiKey: ('AIzaSyAE5vqzsQ74DRmQu3Cyhf3k-n8lNr7by3c')
})(MapContainer);