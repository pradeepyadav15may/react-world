import {connect} from 'react-redux';
import MyPostListComponent from '../components/myPostsListComponent';

const mapStateToProps = (state) => ({
    postsList: state.postsList,
    }
);

export default connect(mapStateToProps)(MyPostListComponent);
