import React, {Component} from 'react';
import MapContainer from '../containers/usersMapContainer';

class MyPostComponent extends Component {
    render() {
        const postsList = this.props.postsList.map((post, key) => {
            return (
                <li key={key}>
                    <span className="postText">
                        {post.postValue}
                    </span>
                    <MapContainer />
                </li>
            )
        });

        return (
            <div className="posts-list-wrapper">
                <ol>
                    {postsList}
                </ol>
            </div>
        )
    }
}


export default MyPostComponent;