import React from 'react';

const MyPostComponent = (props) => {    
    return (
        <div className="my-post-wrapper">
            <textarea id="inputTextID" className="inputText" placeholder={props.placeholder} />
            <div className="btn-container">
                <button className="btn btn-primary" onClick={props.postItBtnClick}>
                    {props.postText}
                </button>
                <a className="location-link">{props.locationLinkText}?</a>
            </div>
        </div>
    )
}


export default MyPostComponent;