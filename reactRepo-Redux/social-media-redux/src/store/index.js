import {createStore} from 'redux';
import socialMediaReducer from '../reducers/socialMediaReducer';

const store = createStore(socialMediaReducer);

export default store;