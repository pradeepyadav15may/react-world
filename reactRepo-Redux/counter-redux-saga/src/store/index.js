import {createStore, applyMiddleware} from 'redux';
import {devToolsEnhancer} from 'redux-devtools-extension'
import reducer from '../reducers/counterreducer';
import createSagaMiddleware from 'redux-saga';
import rootSaga from '../saga1';

const sagaMiddleware = createSagaMiddleware();

const store = createStore(reducer, 
    devToolsEnhancer(),
    applyMiddleware(sagaMiddleware)
);

sagaMiddleware.run(rootSaga);
export default store;