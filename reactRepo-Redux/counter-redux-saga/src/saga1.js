//import { watchIncrementAssynctakeEvery} from './saga'
import { call, takeEvery, put } from 'redux-saga/effects';
import { delay } from '../node_modules/redux-saga';

function* incrementAsync (){
    yield call (delay, 2000)
    yield put({type:'INCREMENT'})

}

function* watchIncrementAssynctakeEvery(){
    yield takeEvery ('INCREMENT_ASYNC',incrementAsync)
}

export default function* roorSaga(){
    yield watchIncrementAssynctakeEvery()
}