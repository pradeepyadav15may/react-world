import {connect} from 'react-redux';
import CounterApp from '../components/counterapp';

const mapStateToProps = state1 => ({
  count: state1.count
})

const mapDispatchToProps = dispatch => {
  return {
    increment: () => dispatch({
      type: 'INCREMENT_ASYNC'
    }),
    decrement: () => dispatch({
      type: 'DECREMENT'
    })
  }
}

const VisibleCounterApp = connect(mapStateToProps, mapDispatchToProps)(CounterApp);
export default VisibleCounterApp;