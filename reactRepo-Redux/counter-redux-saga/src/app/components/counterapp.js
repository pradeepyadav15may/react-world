import React from 'react';
import Increment from './increment';
import Decrement from './decrement';
import Result from './result';


const CounterApp = props => {
    
        return (
             <div className="container">
                 <Increment increment={props.increment} />
                 <Result count={props.count} />
                 <Decrement  decrement={props.decrement}/>
             </div>
        );
  
}

export default CounterApp;

