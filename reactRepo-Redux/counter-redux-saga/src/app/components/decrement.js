import React from 'react';

const Decrement = props => {
  return ( 
    <button className = "btn btn-primary" onClick={props.decrement}> - </button>
  )
}

export default Decrement;