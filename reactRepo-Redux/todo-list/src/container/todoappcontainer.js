import { connect } from "react-redux";
import TodoAppContainer from "../components/todoApp";

const mapStateToProps = state => ({
        "tasks" : state.tasks,
        "viewType" : state.viewType,
        "newTask" : state.newTask,
})
const mapDispatchToProps = dispatch => {
    return {
        addtask: () => dispatch({
            type: 'ADDTASK'
        }),
        inputTask: (e) => dispatch({
            type: 'INPUTTASK',
            payload: e.target.value,
        })
    }
}

const todoappcontainer = connect(mapStateToProps, mapDispatchToProps )(TodoAppContainer);
export default todoappcontainer;

//#TM: Looks good