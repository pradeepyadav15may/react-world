import React, {Component} from 'react';
import '../App.css';

export default class ToggleViews extends Component {
    render () {
        return (
            <div >
                <button 
                    name="Show Completed Tasks" 
                    value="closed" 
                    onClick={this.props.toggleTasks}
                >
                    Show Completed Tasks
                </button>

                <button 
                    name="Show Open Tasks" 
                    value="open" 
                    onClick={this.props.toggleTasks}
                >
                    Show Open Tasks
                </button>
                <button 
                    name="Show Open Tasks" 
                    value="All" 
                    onClick={this.props.toggleTasks}
                >
                    All Tasks
                </button>
            </div>
        );
    }
}
//TM: Looks good.