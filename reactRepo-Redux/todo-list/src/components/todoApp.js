import React from 'react';
import '../App.css';
import AddBtn from './addBtn';
import TasksView from './tasksView';
import ToggleViews from './toggleViews';

const TodoApp = (props) => {
    
    return (
        <div >
            <AddBtn 
                addTask={props.addTask} 
                tasks={props.tasks} 
                inputTask={this.taskInput}
                toggleTaskState={this.toggleTaskState}
            />
            <TasksView 
                tasksList={props.tasks}
                viewType={props.viewType}
                toggleTaskState={this.toggleTaskState}
            />
            <ToggleViews toggleTasks={this.toggleTasks}/>
        </div>
    );
}

export default TodoApp;
//TM: Looks good.