import React from 'react';

const TasksView = (props) => {
    console.log(props);
    const viewType = props.viewType;
    const list = props.tasksList.map((task, index)=>{
        if(viewType === "All") {
            return (
                <li 
                    key={index} 
                    onClick={props.toggleTaskState} 
                    taskkey={index} 
                    className={task.taskStatus}
                >
                    {task.taskName}
                </li>
            )
        }else if(viewType === task.taskStatus) {
            return (
                <li 
                    key={index} 
                    onClick={props.toggleTaskState} 
                    taskkey={index} 
                    className={task.taskStatus}
                >
                    {task.taskName}
                </li>
            )
        }
        return "";
    });

    return (
        <div>
            <ol>
                {list}
            </ol>
        </div>
    )
}


export default TasksView;

//TM: Looks good.