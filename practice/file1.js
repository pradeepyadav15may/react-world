var greet = function(x) { return `Hello, ${ x }` };
var emote = function(x) { return `${ x }` };
var compose = function(f,g) {return function(x) {return f(g(x));}};
// var happyGreeting= compose(greet, emote);
// happyGreeting("Mark");

console.log(greet(emote("Monu")));
//Output will be Hello, Mark: TekMentors

