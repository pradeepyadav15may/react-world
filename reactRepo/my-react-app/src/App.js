import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Clock from './components/clock';
import Counter from './components/counter';
import PalleteCard from './components/palleteCard';
import AnotherCounter from './components/anothercounter';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <Clock currentDate={(new Date()).toLocaleString('en-GB', { timeZone: 'UTC' })}/>
        <Counter myCounter={0}/>
        <AnotherCounter newCounter={0}/>
        <PalleteCard />
      </div>
    );
  }
}

export default App;

/*
  - Check for Styled Components
  - Check for React ref ????
  - Check for JSON Server, how to make use of it ?
  - When we want data to flow from parent to child, props is used.
  - Where mutability is desired use state.
  - CSS-in-JS is preferred way in React, coz acn be used natively used in web & react-native both.
  - When all child's render finishes then only parent's render completes.
    - 1-Functionaly depompose the problem first. Means component heirarchy will be what?
    - 2-What is Controlled component & Un-Controlled component?
    - 3-Check for "Thinking In React", in React's official site.
    - 4-What is SOLID in design?
  - render() is not mounting event.
  - who is Matin from thoughtworks ??
  - Why we return new state always ? because of shallow referencing in react. i.e. even if you change the internal structure of the state, it's hex value 
    is not changed that means reference is not changed so RENDER will not be called. so to forcefully call the render we return new state always.
*/