import React, { Component } from 'react';
import IncrementBtn from './increment';
import DecrementBtn from './decrement';
import CounterView from './counterView';

export default class Counter extends Component {
    constructor (props) {
        super(props);
        this.state = {
            currentTime: props.currentDate,
            myCounter: 0,
        }
        //this.incrementHandler = this.incrementHandler.bind(this);
    }
    incrementHandler = () => {
        this.setState ({
            myCounter: this.state.myCounter + 1,
        });
    }

    decrementHandler = () => {
        this.setState ({
            myCounter: this.state.myCounter - 1,
        });
    }

    render () {
        return (
            <div className="counterContainer">
                <IncrementBtn incrementHandler11={this.incrementHandler} btnName="Mera wala purana Increment"/>
                    <CounterView counter={this.state.myCounter}/>
                <DecrementBtn decrementHandler11={this.decrementHandler}/>
            </div>
        );
    }
} 

//TM: Looks good.