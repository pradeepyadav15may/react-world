import React, { Component } from 'react';

export default class Clock extends Component {
    constructor (props) {
        super(props);
        this.state = {
            currentTime: props.currentDate,
        }
    }
    render () {
        return (
            <h1> {this.state.currentTime} </h1>
        );
    }
}

//TM: Looks good.