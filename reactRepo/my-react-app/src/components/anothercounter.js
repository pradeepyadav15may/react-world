import React, { Component } from 'react';
import IncrementBtn from './increment';
import CounterView from './counterView';

export default class AnotherCounter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            myCounter: props.newCounter,
        }
    }
    incrementHandler = () => {
        this.setState ({
            myCounter: this.state.myCounter + 1,
        });
    }

    render () {
        return (
            <div className="counterContainer">
                <IncrementBtn incrementHandler11={this.incrementHandler} btnName="Mera Naya Increment"/>
                    <CounterView counter={this.state.myCounter}/>
            </div>
        );
    }
} 

//TM: Looks good.