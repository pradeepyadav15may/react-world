import React from 'react';

const PalleteHeader = (props) => {
    return (
        <div className="palleteHeader" style={{backgroundColor: props.headerColor}}>
            <span>
                {props.headerColor}
            </span>          
        </div>
    );
}
export default PalleteHeader;

/*
    It is Stateless/Dumb/Presentational component.
*/


//TM: Looks good.