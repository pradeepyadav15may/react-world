import React, { Component } from 'react';

export default class IncrementBtn extends Component {

    render() {
        return (
            <button 
                value="increment" 
                className="increment" 
                onClick={this.props.incrementHandler11}
            >
            {this.props.btnName} 
            </button>
        );
    }
}


//TM: Looks good.