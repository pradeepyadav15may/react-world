import React from 'react';

const PalleteFooter = (props) => {
    return (
        <div className="palleteFooter" style={{backgroundColor: props.footerColor}}>
            <span>
                {props.footerColor}
            </span>
        </div>
    );
}
export default PalleteFooter;

/*
    It is Stateless/Dumb/Presentational component.
*/


//TM: Looks good.