import React, {Component} from 'react';
import PalleteHeader from './palleteHeader';
import PalleteFooter from './palleteFooter';

class PalleteCard extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            initialPalleteHeaderColor: '#F6BB42',
            initialPalleteFooterColor: '#37BC9B',
            cards : [
                {
                    initialPalleteHeaderColor: '#F6BB42',
                    initialPalleteFooterColor: '#37BC9B',
                    text:'abc'
                },
                {
                    initialPalleteHeaderColor: 'teal',
                    initialPalleteFooterColor: 'blue',
                    text:'cde'
                },
                {
                    initialPalleteHeaderColor: 'grey',
                    initialPalleteFooterColor: 'lightGrey',
                    text:'abc'
                },
                {
                    initialPalleteHeaderColor: '#F6BB42',
                    initialPalleteFooterColor: '#37BC9B',
                    text:'cde'
                }
            ]
        }
    }
    
    render() {
        const renderedItems = this.state.cards.map((card, key) => {
            return (
                <div className="palleteCardContainer" index={key}>
                    <PalleteHeader headerColor={card.initialPalleteHeaderColor}/>
                    <PalleteFooter footerColor={card.initialPalleteFooterColor}/>
                </div>);
            });
            return renderedItems;
        }
}
export default PalleteCard;

//TM: Looks good.
