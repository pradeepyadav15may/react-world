import React from 'react';

const CounterView = (props) => {
    return (
        <label className="counterLabel">{props.counter}</label>
    );
}
export default CounterView;

/*
    It is Stateless/Dumb/Presentational component.
*/


//TM: Looks good.