import React, {Component} from 'react';
import MyPost from './myPost';
import AllPosts from './all-posts';

export default class SocialMediaContainer extends Component {
    constructor(props) {
        super(props);
        this.placeholder = "What's going on?";
        this.postBtnText = "Post kar de";
        this.addLocationText = "Kahan hai?";
        this.state = {
            loggedIn: true,
            myPosts: [],
        };
    }
    
    render () {
        return (
            <div className="social-media-container">
                <MyPost 
                    placeholder={this.placeholder} 
                    postBtnText={this.postBtnText}
                    locationLinkText={this.addLocationText}
                />
                <AllPosts />
            </div>
        );
    }
}