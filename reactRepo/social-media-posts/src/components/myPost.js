import React, {Component} from 'react';

export default class MyPost extends Component {
    constructor (props) {
        super(props);
        this.state = {
            postingSomething: false,
        }
    }
    render() {
        return (
            <div className="my-post-container">
                <div className="post-text-wrapper">
                    <textarea className="input-post" placeholder={this.props.placeholder}/>
                </div>
                <div className="post-action-wrapper">
                    <button className="post" >{this.props.postBtnText}</button>
                    <a className="add-location" href="#">{this.props.locationLinkText}</a>
                </div>
            </div>
        );
    }
}