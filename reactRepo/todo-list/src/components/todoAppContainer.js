import React, {Component} from 'react';
import '../App.css';
import AddBtn from './addBtn';
import TasksView from './tasksView';
import ToggleViews from './toggleViews';

export default class TodoAppContainer extends Component {
    constructor (props){
        super(props);
        this.state = {
            "tasks" : [],
            "viewType" : "All",
            "newTask" : "",
        }
    }
    taskInput = (e) => {
        this.setState ({
            newTask : e.target.value,
        });
    }
    addTask = () => {
        const newTask = {
            "taskName" : this.state.newTask,
            "taskStatus" : "open"
        }
        this.setState ({
            "tasks" : [...this.state.tasks, newTask]
        });
    }
    toggleTaskState = (e) => {
        //console.log(this.state.tasks[e.target.getAttribute('taskkey')]);
        const newStateOfTasks = this.state.tasks.map((task, index)=>{
            if(index === parseInt(e.target.getAttribute('taskkey'), 10)) {
                if(task.taskStatus === "open") {
                    task.taskStatus = "closed";
                }else {
                    task.taskStatus = "open";
                }
            }
            return task;
        }, {});

        this.setState({
            "tasks": newStateOfTasks
        });
    }
    toggleTasks = (e) => {
        this.setState({
            viewType: e.target.getAttribute('value')
        });
    }
    render () {
        return (
            <div >
                <AddBtn 
                    addTask={this.addTask} 
                    tasks={this.state.tasks} 
                    inputTask={this.taskInput}
                    toggleTaskState={this.toggleTaskState}
                />
                <TasksView 
                    tasksList={this.state.tasks}
                    viewType={this.state.viewType}
                    toggleTaskState={this.toggleTaskState}
                />
                <ToggleViews toggleTasks={this.toggleTasks}/>
            </div>
        );
    }
}

//TM: Looks good.