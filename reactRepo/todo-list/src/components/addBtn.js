import React, {Component} from 'react';
import '../App.css';

export default class AddBtn extends Component {
    render () {
        return (
            <div >
                <input 
                    type="text" 
                    className="inputTask" 
                    id="taskInput" 
                    onChange={this.props.inputTask}
                />
                <button 
                    name="Add Task" 
                    value="Add My Task" 
                    onClick={this.props.addTask}
                >
                    Add My Task
                </button>
            </div>
        );
    }
}

//TM: Looks good.