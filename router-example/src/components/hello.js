import React from 'react';

const HelloComponent = () => {
    return(
        <div className="jumbotron">
            <h1 className="display-3">Hello Component</h1>
        </div>
    )
}

export default HelloComponent;