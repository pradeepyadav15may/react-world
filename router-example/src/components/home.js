import React from 'react';

const HomeComponent = () => {
    return(
        <div className="jumbotron">
            <h1 className="display-3">Home Component</h1>
        </div>
    )
}

export default HomeComponent;