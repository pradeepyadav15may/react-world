'use strict';
//  Exercise 1-Write an arrow function that returns the string ‘Hello World’

const helloWorld1 = () => "Hello World1";
console.log(helloWorld1());

const helloWorld2 = (param1) => param1;
console.log(helloWorld2("Hello World2"));

const helloWorld3 = (param1) => "Hello " + param1;
console.log(helloWorld3("Hello World3"));

// TODO - what is the issue? prints "Hello ${param1}"
const helloWorld4 = (param1) => 'Hello ${param1}';
console.log(helloWorld4("Hello World4")); 

//  Exercise 2-Write an arrow function that expects an array of integers, 
//  and returns the sum of elements of the array. 
//  Use the built-in method reduce on the array argument

const sumOfArray = (arr) => {
    return arr.reduce((total, next) => {
        return total + next;
    });
};
sumOfArray([1,1,2,2]);

