//  1-  ==================================================
//  Given this array: `[3,62,234,7,23,74,23,76,92]`, use an arrow function to create an array of the numbers greater than `70` ?

    let arr = [3, 62, 234, 7, 23, 74, 23, 76, 92];
    const filterMethod = (num) => {
        return num > 70 ? num : '';
    };
    let arrGreaterThen70 = arr.filter(filterMethod);
    console.log(arrGreaterThen70);

//  Good Alternate Solution
const numbers = [3, 62, 234, 7, 23, 76, 92];

const greaterThanFilter = (num, numToCompareWith) => num > numToCompareWith;

let numbergreaterthan70 = numbers.filter(n => greaterThanFilter(n,70));
console.log(numbergreaterthan70);


//  ==================================================
//  2-
//  Write an expression using higher order array methods to compute the total value of 
//  machines in the inventory array (Using Arrow Function)

    const inventory = [{
        type: "machine",
        value: 5000
    }, {
        type: "machine",
        value: 650
    }, {
        type: "duck",
        value: 10
    }, {
        type: "furniture",
        value: 1200
    }];
    const machineFilter = (item) => {
        return item.type === "machine";
    };
    const reducer = (next, value) => {
        return next + value;
    };

    inventory.filter(machineFilter)
    .reduce((accumulator, next) => {
        return accumulator + next.value;
    }, 0);
//  ==================================================
//  3-
//  Create a function than when executed as follows mergeWords(‘There’)(‘is’)(‘no’)(‘water.’)(); should return ‘There is no water.’

    let mergeWords = (a) => (b) => (c) => (d) => a + " " + b + " " + c + " " + d;
    console.log(mergeWords('There')('is')('no')('water.'));

// #TM: Use recursive way of doing this. This solution assumes that you have to merge only 4 words.

//  Using Recursion - TODO
//  ==================================================

//  Given that const humans = ["Rohan", "Mohit", "Sanjay"];
//  Write a function that prints following message for each name in the array. 
//  [Rohan is of human type] ==== & === [Mohit is of human type] ==== & === [Sanjay is of human type].
    const humans = ["Rohan", "Mohit", "Sanjay"];
    const type = (type) => type;

    let temp = (human) => {
        return (type) => {
            console.log(human + " is of " + type + " type.");
        }
    }

    humans.forEach(element => {
        temp(element)(type("humans"));
    });
//  ==================================================
//  TODO
//  1.  Create an implementation where we can extract substring from a given string
//          a) str.substr(start, length)
//  2.  Modify the Program to find the first nth characters
//  3.  Modify the Program to find the first character from a given string.

    let str = "hello, how are you ?"


//  ==================================================
//  Create an array that contains the last four characters of another array. 
//  Use ES6 construct like Spread/Rest concept

    let arr = ["a", "b", "x", "y", "p", "q", "r", "s"];
    [, , , , ...tempArr] = arr;
    console.log(...tempArr);
//  ==================================================

//  Given that 
    const species = [{
        name: "Mohan",
        type: "human"
    }, {
        name: "Sanjay",
        type: "human"
    }, {
        name: "Thanos",
        type: "alien"
    }];
//  TODO - 
//  1.Write a function that list only those records which are human
//  Use Currying technique to solve this

    let type = (type) => type;

    let temp = (human) => {
        return (type) => {
            return human.type == type;
        }
    }

    species.filter(temp()("human"));

//  ==================================================
//  Define a function that concatenates several strings
//  –Function should take a separator and list of arguments
//  –Function should return list of arguments concatenated and separated by separator.

    let myString = (separator, ...args) => {
        return args.map((value, index) => {
            return value + separator;
        });
    }
    console.log(myString(":", "A", "B", "hello", "jaljaslkdjasl"));
//  O/P - ["A:","B:","hello:","jaljaslkdjasl:"] 

//  ==================================================
//  Define a function that creates HTML lists
//  –Creates a string containing HTML for a list
//  –Only formal argument for a function is a string that is “u” for unordered list and “o” for ordered list

    let createList = (type) => {
        let orderedList = `
            <ol>
                <li></li>
                <li></li>
            </ol>
        `;
        let unOrderedList = `
            <ul>
                <li></li>
                <li></li>
            </ul>
        `;

        return (type === "o") ? orderedList : unOrderedList;
    }
    console.log(createList ("o"));

//  ==================================================

//  Create a factorial calculator

    let fact = (n) => n>=1 ? n*fact(n-1) : 1;
    fact(5); // 120
    fact(4); // 24
//  ==================================================

//  Write a function that executes a callback function after a given delay in milliseconds. 
//  The default value of delay is one second. 
//  Use ES6 Construct as far as possible
    let callbackFunc = ()=>console.log('hi');

    let delayFunc = (myCallback, timer) => setInterval(() => {myCallback()}, timer);

    delayFunc(callbackFunc, 2000);

//  ==================================================

//  Change the below code such that the second argument of printComment has a default value that’s initially 1, and is incremented by 1 after each call. 
    function printComment( comment, line ) { 
        console.log( line, comment ); 
    }

    //Solution TODO-
    let printComment = ( comment, line=1 ) => { 
        console.log( ++line, comment ); 
    }
//  ==================================================
//  Determine the values written to the console.
    function argList( productName, price = 100 ) {
        console.log( arguments.length);
        console.log( productName=== arguments[0] );
        console.log( price === arguments[1] );
    };
    argList( 'Krill Oil Capsules' );
//  o/p -
    1
    true
    false

//  ==================================================
//  Rewrite the sumArgs function of this tutorial in ES2015, using a rest parameter and arrow functions. 
    function sumArgs() {
        var result = 0;
        for( var i= 0; i< arguments.length;  + +i) {
            result += arguments[i];
        }
        return result;
    }

    console.log(sumArgs(2,5,7,9));

    let sumArgs = (...args) => {
        let result = 0;
        for( let i= 0; i< args.length;  ++i) {
            result += args[i];
        }
        return result;
    }
    console.log(sumArgs(2,5,7,9));

//  ==================================================
//  Create a 10 x 10 matrix of null values
// TODO
    let matrix = () => {
        
    }

//  ==================================================
//  Write function that accepts two String arguments, and returns the length of the longest common substring
// in the two strings. 
// TODO
    maxCommon( '523', '5' ); // 1 
    maxCommon( '55444', '55f444g'); // 3 
    maxCommon( 'abc', '334cab' ); // 2 


//  ==================================================
//  Swap the value of variables
    var a = 5;
    var b = 7;
    [a, b] = [b, a];

    console.log(a);
    console.log(b);

//  ==================================================
//  Write a parseProtocol(url) which takes url as parameter and returns the protocol for me Like below:-
//  console.log(parseProtocol(‘https://sytamatik.com/en-US/Web/Home’));
//  Result returned should be https
//  •Modify the method to return fullurl, protocol and host part separately

    let parseProtocol = (url) => {
        let [url, protocol, host, ...others] = /^(\w+)\:\/\/([^\/]+)\/(.*)$/.exec(url);
        console.log(url);
        console.log(protocol);
        console.log(host);
    }
    console.log(parseProtocol('https://sytamatik.com/en-US/Web/Home'));

//  ==================================================
//  Swap two variables using one destructuring assignment
    let temp = {item1: 1, item2: 2};
    ({item2:item1, item1:item2} = temp);
    console.log(item1);
    console.log(item2);

//  ==================================================
//  Complete the below function that calculates the nth fibonacci number in the sequence 
//  with one destructuring assignment. Definition of Fibonacci number is the following
//  –fib(0)=0
//  –fib(1)=1
//  –fib(n)=fib(n-1)+fib(n-2);

    function fib( n ) { 
        let fibCurrent= 1; 
        let fibLast= 0; 
        if ( n < 0 ) return NaN; 
        if ( n <= 1 ) return n; 
        for ( let fibIndex= 1; fibIndex< n; ++fibIndex) { 
            // Insert one destructuring expression here 
            [fibCurrent, fibLast] = [fibLast, fibCurrent + fibLast];
        }
        return fibCurrent;     
    }
    console.log(fib(10));

//  ==================================================
//  Suppose the following configuration object of a financial chart is given: 
let config= { 
    chartType: 0,
    bullColor: 'green',
    bearColor:  ' red ',
    days : 30 
}; 
//  Complete the function signature below such that the function may be called with any config objects 
//  (null and undefined are not allowed as inputs). 
//  If any of the four keys are missing, substitute their default values. 
//  The default values are the same as in the example configuration object. 
    function drawChart( {chartType=0}, {bullColor='green'}, {bearColor='red'}, {days=30}) { 
        // do not implement the function body 
    };

//  ==================================================
//  Create one destructuring expression that declares exactly one variable to retrieve x.A[2]
    let x = { A: ['t’, ‘e’, ‘s’, ‘t']};
    ({A:a} = x)
    console.log(a);
//  TODO
//  ==================================================








// #TM : looks like Java script and ES6 are new to you. Please feel free to discuss with us if there is any gap.

