import EventsList from '../components/event/EventsList';
import { connect } from 'react-redux';
//  import { getEvents, deleteEvent, editEvent } from '../actions/eventActions';


const mapStateToProps = state => ({
    events : state.events
});

const mapDispatchToProps = (dispatch) => {
    return {
        delete: (e) => dispatch({
            type: 'DELETE_EVENT',
            payload: e.target
        }),
        editEvent: (e) => dispatch({
            type: 'EDIT_EVENT',
            payload: e.target
        }),
        getEvents : () => dispatch({
            type: 'GET_EVENTS'
        })
    }
};

const EventListContainer = connect(mapStateToProps, mapDispatchToProps)(EventsList);

export default EventListContainer;