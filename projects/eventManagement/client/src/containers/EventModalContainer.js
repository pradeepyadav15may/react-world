import { connect } from 'react-redux';
import EventModal from '../components/EventModal';

const mapStateToProps = state => ({
    event : state.event,
    modal : state.modal
});

const mapDispatchToProps = (dispatch) => {
    return {
        toggle: (e) => dispatch({
            type: 'TOGGLE'
        }),
        onBlur: (e) => dispatch({
            type: 'ON_BLUR',
            payload: e.target,
        }),
        onSubmit: (e) => dispatch({
            type: 'ON_SUBMIT',
            payload: e,
        }),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(EventModal);