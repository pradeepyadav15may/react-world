import { createStore, applyMiddleware } from 'redux';
import {devToolsEnhancer} from 'redux-devtools-extension'
//import rootReducer from '../reducers/index';
import eventReducer from '../reducers/eventReducer.js';
import createSagaMiddleware from 'redux-saga';
import rootSaga from '../saga'

const saga = createSagaMiddleware();

const store = createStore(eventReducer, devToolsEnhancer(),applyMiddleware(saga));


saga.run(rootSaga);
export default store;