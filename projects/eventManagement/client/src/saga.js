import { takeLatest, call, put} from 'redux-saga/effects'
import { takeEvery } from 'redux-saga'
import axios from "axios";

/* export const delay = ms => new Promise(resolve => setTimeout(resolve, ms))

function* addTaskAsync() {
    yield call(delay, 1000)
    yield put({type : 'ADDTASK'})
}

function* watchAddTaskAsyncTakeEvery() {
    yield takeEvery('ADDTASk_ASYNC', addTaskAsync)
}

function* changeStatusAsync() {
    yield call(delay, 1000)
    yield put({type : 'CHANGESTATUS'})
}

function* watchChangeStatusAsyncTakeEvery() {
    yield takeEvery('CHANGESTATUS_ASYNC', changeStatusAsync)
} */


// function that makes the api request and returns a Promise for response
function fetchEvents() {
    return axios({
      method: "get",
      url: "http://localhost:5000/events"
    });
  }

  // function that makes the api request and returns a Promise for response
function updateEvents() {
    return axios({
      method: "put",
      url: "http://localhost:5000/events"
    });
}
function addEvents(action) {
  return axios({
    method: "post",
    data: action.payload,
    url: "http://localhost:5000/events"
  });
}
export function* watchAddEvents() {
  yield takeLatest("ON_SUBMIT", workerSubmitEvents);
}
function* workerSubmitEvents() {
  try {
    const response = yield call(addEvents);
    const events = response.data;
    console.log()

    // dispatch a success action to the store with the new dog
    yield put({ type: "GET_EVENTS_SUCCESS", events });
  
  } catch (error) {
    // dispatch a failure action to the store with the error
    yield put({ type: "API_CALL_FAILURE", error });
  }
}

// watcher saga: watches for actions dispatched to the store, starts worker saga
export function* watchGetEvents() {
  yield takeLatest("GET_EVENTS", workerGetEvents);
}

// worker saga: makes the api call when watcher saga sees the action
function* workerGetEvents() {
  try {
    const response = yield call(fetchEvents);
    const events = response.data;
    console.log()

    // dispatch a success action to the store with the new dog
    yield put({ type: "GET_EVENTS_SUCCESS", events });
  
  } catch (error) {
    // dispatch a failure action to the store with the error
    yield put({ type: "API_CALL_FAILURE", error });
  }
}

export default function* rootSaga() {
    yield [
    watchGetEvents(),
    watchAddEvents(),
		//watchChangeStatusAsyncTakeEvery()
	]
}