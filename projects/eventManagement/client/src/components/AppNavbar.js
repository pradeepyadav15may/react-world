import React, { Component } from 'react';
import auth from './auth';
import {PrivateRoute} from './privateroute';
import { Link} from 'react-router-dom';
import LoginForm from './LoginForm';
import Register from './Register';
import EventListContainer from '../containers/EventListContainer';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    Container
  } from 'reactstrap';


export default class AppNavbar extends Component {
    state = {
        isOpen: false
      };
    
      toggle = () => {
        this.setState({
          isOpen: !this.state.isOpen
        });
      };
      
    render() {
    return (
        <div>
            <Navbar color="dark" dark expand="sm" className="mb-5">
                <Container>
                    <NavbarBrand href="http://localhost:3000/login" >
                        <img src="../images/eventLogo.jpg" height="33" width="120" alt="Event Management" />
                    </NavbarBrand>
                    <NavbarToggler onClick={this.toggle} />
                    <Collapse isOpen={this.state.isOpen} navbar>
                        <Nav className="ml-auto" navbar>
                            <NavItem>
                            <Link to="/login">
                                    Sign-In
                            </Link>
                                
                            </NavItem>
                            <NavItem>
                            <Link to="/Register">
                                    Sign-Up
                            </Link>
                                
                            </NavItem>
                            <NavItem>
                            <Link to="/eventListContainer">
                                    Event List
                            </Link>
                                
                            </NavItem>
                        </Nav>
                    </Collapse>
                </Container>
            </Navbar>
        </div>
    )   
    };
}
