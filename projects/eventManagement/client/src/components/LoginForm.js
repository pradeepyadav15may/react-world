import React, {Component} from 'react';
import auth from './auth';
import {Redirect} from 'react-router-dom';
import { Col, Container, Button, Form, FormGroup, Label, Input } from 'reactstrap';

export default class LoginForm extends React.Component {
  constructor() {
    super();
    this.state = {
      redirectToReferrer: false,
      username : '',
      userPassword: ''
    }
    // binding 'this'
    this.login = this.login.bind(this);
  }
  handleChangeUsername = (e) => {
    this.setState({username: e.target.value})
  }
  handleChangePassword = (e) => {
    this.setState({userPassword: e.target.value})
  }
  login() {
    console.log('login : ',this.state.username,this.state.userPassword);
    auth.authenticate(this.state.username, this.state.userPassword,() => {
      this.setState({ redirectToReferrer: true })
    })
  }

  render() {
    const { redirectToReferrer } = this.state;
    console.log('redirectToReferrer : ',redirectToReferrer, auth.isAuthenticated)
    if (auth.isAuthenticated && redirectToReferrer) {
      const from = {
        pathname: '/EventListContainer',
        key: 'abcd'
      }
      return (
        <Redirect to={from} />
      )
    }

    return (
      <Container>
      <Form>
      <FormGroup row>
          <Label for="exampleEmail" sm={2}>UserName</Label>
          <Col sm={10}>
            <Input type="text" onChange={this.handleChangeUsername} name="email" id="exampleEmail" placeholder="UserName" />
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="exampleEmail" sm={2}>Password</Label>
          <Col sm={10}>
            <Input type="password" onChange={this.handleChangePassword} ref={node => this.password=node} name="email" id="exampleEmail" placeholder="Enter Password" />
          </Col>
        </FormGroup>
        <FormGroup check row>
          <Col sm={{ size: 10, offset: 2 }}>
            <Button onClick={this.login}>Submit</Button>
          </Col>
        </FormGroup>
      </Form>
      </Container>
    );
  }
}