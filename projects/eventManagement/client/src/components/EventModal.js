import React, { Component } from 'react'
import {
    Button,
    ButtonGroup,
    Modal,
    ModalHeader,
    ModalBody,
    Form,
    FormGroup,
    Label,
    Input
} from 'reactstrap';

class EventModal extends Component {
    
    // eventInfo = () => {
    //     const event = this.props.event;
    //     // console.log(event);
    //     // console.log('event');
    //     const tmp = {};
    //     if(event.length) {
    //         tmp.eventName = event[0].eventName;
    //         tmp.eventVenue = event[0].eventVenue;
    //         tmp.eventDate = event[0].eventDate;
    //     }
    //     return tmp;
    // }


    render() {
        // console.log(this.eventInfo());
        console.log('EventModal.js ', this.props);
        return (
            <div>
                <Button 
                    color="dark"
                    style = {{marginBottom : '2rem'}}
                    onClick = {this.props.toggle}
                >
                    Add Event
                </Button>

                <Modal
                    isOpen = {this.props.modal}
                    toggle = {this.props.toggle}
                >
                <ModalHeader 
                    toggle = {this.props.toggle}>
                    ADD/Edit Event
                </ModalHeader>
                <ModalBody>
                    <Form onSubmit = {this.props.onSubmit}>
                        <FormGroup>
                            <Label for="event">Event Name</Label>
                            <Input
                                type="text"
                                id="eventName"
                                name="eventName"
                                placeholder = "Event Name"
                                onBlur = {this.props.onBlur}
                                defaultValue={this.props.event.eventName}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="event">Event Venue</Label>
                            <Input
                                type="text"
                                id="eventVenue"
                                name="eventVenue"
                                placeholder = "Event Venue"
                                onBlur = {this.props.onBlur}
                                defaultValue={this.props.event.eventVenue}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="event">Event Date</Label>
                            <Input
                                type="text"
                                id="eventDate"
                                name="eventDate"
                                placeholder = "Event Date"
                                onBlur = {this.props.onBlur}
                                defaultValue={this.props.event.eventDate}
                            />
                        </FormGroup>
                        <ButtonGroup style={{marginTop : '2rem'}}>
                            <Button 
                                color="dark"
                                onClick = {this.props.onSubmit}
                            >Submit</Button>
                            <Button 
                                color="light"
                                style={{marginLeft: '2rem'}}
                                onClick = {this.props.toggle}
                            >Cancel</Button>
                        </ButtonGroup>
                    </Form>
                </ModalBody>
                </Modal>
            </div>
        )
    }
}

export default EventModal;