import React from 'react'
import { Button } from 'reactstrap';

/* import { getItems, deleteItem } from '../actions/itemActions';
import PropTypes from 'prop-types'; */

const Event = (props) => {
    console.log("props inside event : ", props.index)
    return (
        <tr>
            <th scope="row">{props.index}</th>
            <td>
                <a href="#" id={props.eventDetails._id}>{props.eventDetails.name}</a>
            </td>
            <td>{props.eventDetails.venue}</td>
            <td>{props.eventDetails.date}</td>
            <td>
                <Button onClick = {props.onEditClick} data-eventid={props.eventDetails._id}>Edit</Button>
                <Button style={{marginLeft: '1rem'}} onClick={props.onDeleteClick} id={props.eventDetails._id}>Delete</Button>
                <Button style={{marginLeft: '1rem'}}>Register Link</Button>
            </td>
        </tr>
    )
}

export default Event;
