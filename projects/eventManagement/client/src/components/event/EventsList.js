import React, { Component } from 'react'
import { Container, Table } from 'reactstrap';
import PropTypes from 'prop-types';
import EventModalContainer from '../../containers/EventModalContainer';
import Event from './Event';
//import { getEvents  } from '../../actions/action-types';

export default class EventsList extends Component {
    componentDidMount(){
        //console.log('componentDidMount : ',this.props)
        this.props.getEvents();
    }

  render() {
    //  console.log(this.props);
    const { events } = this.props;
    console.log('eventlist : ',events);
    return (
      <Container>
        <Table striped>
            <thead>
            <tr>
                <th>#</th>
                <th>Event Name</th>
                <th>Event Venue</th>
                <th>Event Date</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
                {
                    events.map((event, index) => (
                            <Event 
                                key = {index} 
                                index = {index+1}
                                eventDetails = {event} 
                                onDeleteClick = {this.props.delete} 
                                onEditClick = {this.props.editEvent}
                            />
                        )
                    )
                }
            </tbody>
        </Table>
        <EventModalContainer />
      </Container>
    )
  }
}
