import React, {Component} from 'react';
import auth from './auth';
import {Redirect} from 'react-router-dom';
import { Col, Container, Button, Form, FormGroup, Label, Input } from 'reactstrap';

export default class Register extends React.Component {
  constructor() {
    super();
    this.email='';
    this.name='';
    this.password='';
    this.state = {
      redirectToReferrer: false
    }
    // binding 'this'
    this.login = this.login.bind(this);
  }
  login() {
    auth.authenticate(this.email.value, this.password.value,() => {
      this.setState({ redirectToReferrer: true })
    })
  }

  render() {
    const { redirectToReferrer } = this.state;
    if (auth.message==='' && redirectToReferrer) {
      const from = {
        pathname: '/EventListContainer',
        key: 'abcd'
      }
      return (
        <Redirect to={from} />
      )
    }

    return (
      <Container>
      <Form>
        {/* <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
          <Label for="email" className="mr-sm-2">Email</Label>
          <input type="text" ref={node => this.email=node} placeholder="email" />
        </FormGroup>
        <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
          <Label for="name" className="mr-sm-2">Name</Label>
          <input type="text" ref={node => this.name=node} placeholder="name" />
        </FormGroup>
        <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
          <Label for="password" className="mr-sm-2">Password</Label>
          <input type="password" ref={node => this.password=node} placeholder="password" />
        </FormGroup>
        <a className="btn btn-primary btn-lg" onClick={this.login} role="button">Register & Login</a> */}
          <FormGroup row>
            <Label for="exampleName" sm={2}>Name</Label>
            <Col sm={10}>
              <Input type="text" ref={node => this.name=node} name="email" id="exampleName" placeholder="Enter Name" />
            </Col>
          </FormGroup>
          <FormGroup row>
            <Label for="exampleEmail" sm={2}>Email</Label>
            <Col sm={10}>
              <Input type="text" ref={node => this.email=node} name="email" id="exampleEmail" placeholder="Enter Email" />
            </Col>
          </FormGroup>
          <FormGroup row>
            <Label for="exampleUsername" sm={2}>Username</Label>
            <Col sm={10}>
              <Input type="text" name="email" id="exampleUsername" placeholder="Enter UserNAme" />
            </Col>
          </FormGroup>
          <FormGroup row>
            <Label for="exampleEmail" sm={2}>Password</Label>
            <Col sm={10}>
              <Input type="password" ref={node => this.password=node} name="email" id="examplePassword" placeholder="Enter Your Secret Password" />
            </Col>
          </FormGroup>
          <FormGroup check row>
            <Col sm={{ size: 10, offset: 2 }}>
              <Button onClick={this.login}>Submit</Button>
            </Col>
          </FormGroup>
        </Form>
      </Container>
    );
  }
}