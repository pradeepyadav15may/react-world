import { 
    TOGGLE, 
    ON_BLUR, 
    ON_SUBMIT, 
    GET_EVENTS, 
    ADD_EVENT, 
    DELETE_EVENT, 
    EDIT_EVENT, 
    GET_EVENTS_SUCCESS 
} from "../actions/action-types";
import axios from 'axios';

const initialState = {
    events : [],
    modal: false,
    event: {detail:''}
};

const eventReducer = (state = initialState, action) => {
    const newEvent = {};
    switch(action.type) {
        case GET_EVENTS: 
            return {
                ...state,
            };
        case GET_EVENTS_SUCCESS:
            return {
                ...state,
                events : action.events
            };
        case DELETE_EVENT :
            return {
                ...state,
                events: state.events.filter(event => {
                    console.log(event._id);
                    console.log(parseInt(action.payload.id));
                    return event._id !== action.payload.id
                })
            }; 
         case ADD_EVENT : 
            return {
                ...state,
                modal: !state.modal
            }    
         case EDIT_EVENT :
            return {
                ...state,
                event: state.events.filter(event => event._id == action.payload.dataset.eventid)[0],
                modal: !state.modal
            } 
        case TOGGLE:
            console.log('TOGGLE')
            return {
                ...state,
                modal: !state.modal,
                event: {}
            }
        case ON_BLUR :
            const fieldType = action.payload.id;
            if(fieldType === 'eventName') {
                state.event.name = action.payload.value;
            }else if(fieldType === 'eventVenue') {
                state.event.venue = action.payload.value;
            }else if(fieldType === 'eventDate') {
                state.event.date = action.payload.value;
            }
            console.log(newEvent);
            
            return {
                ...state,
                event: state.event,
            }
        case ON_SUBMIT :
            action.payload.preventDefault();
            return {
                ...state,
                events: [...state.events, state.event],
                modal: !state.modal,
                event: state.event
            } 
        default :
            return state    
    }
}
export default eventReducer;