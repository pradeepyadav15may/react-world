import React, { Component } from 'react';
import {BrowserRouter as Router, Switch, Link, Redirect, Route} from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import EventListContainer from './containers/EventListContainer';
import EventModalContainer from './containers/EventModalContainer';
import LoginForm from './components/LoginForm';
import Register from './components/Register';
import { Container } from 'reactstrap';
import auth from './components/auth';
import {PrivateRoute} from './components/privateroute';

import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css';

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <AppNavbar />
          {/* <Container>
            <LoginForm />
              <EventListContainer />
              <EventModalContainer />
          </Container> */}
          {/* <Container>
            <ul>
              <li><Link to="/login">Login</Link></li>
              <li><Link to="/Register">Register</Link></li>
              <li><Link to="/eventListContainer">Events List</Link></li>
            </ul>
          </Container> */}
            <hr />
            <Switch>
              <Route path="/login" component={LoginForm} />
              <Route path="/register" component={Register} />
              <PrivateRoute authed={auth.isAuthenticated} path="/eventListContainer" component={EventListContainer} />
            </Switch>
            
        </div>
      </Router>
    );
  }
}

export default App;
