export const GET_EVENTS = "GET_EVENTS";
export const GET_EVENTS_SUCCESS = "GET_EVENTS_SUCCESS";
export const ADD_EVENT = "ADD_EVENT";
export const DELETE_EVENT = "DELETE_EVENT";
export const EDIT_EVENT = "EDIT_EVENT";
export const TOGGLE = "TOGGLE";
export const ON_BLUR = "ON_BLUR";
export const ON_SUBMIT = "ON_SUBMIT";