import { GET_EVENTS, ADD_EVENT, DELETE_EVENT, EDIT_EVENT } from "./action-types";

//export const getEvents = article => ({ type: ADD_ARTICLE, payload: article });

export const getEvents = () => {
    return {
        type: GET_EVENTS
    };
};

export const deleteEvent = id => {
    return {
        type: DELETE_EVENT,
        payload: id
    };
};

export const addEvent = (event,modal) => {
    return {
        type: ADD_EVENT,
        payload: {
            event: event,
            modal:modal
        }
    };
};

export const editEvent = (event,modal) => {
    console.log("editEvent::::::::::::::::::::::::::: : ",event,"          ",modal);
    return {
        type: EDIT_EVENT,
        payload: { 
            event: event,
            modal: modal
        }
    };
};