const mongoose = require('mongoose');

const UsersSchema = mongoose.Schema({
    name: String,
    email: {type:String, unique: true},
    username: String,
    userpassword: String
}, {
    timestamps: true
});

module.exports = mongoose.model('Users', UsersSchema);