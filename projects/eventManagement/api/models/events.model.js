const mongoose = require('mongoose');

const EventsSchema = mongoose.Schema({
    name: String,
    venue : String,
    detail : String,
    date : String,
    attendies : Array
}, {
    timestamps: true
});

module.exports = mongoose.model('Events', EventsSchema);