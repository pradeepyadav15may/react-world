module.exports = (app) => {
    const Events = require('../controllers/events.controller.js');

    // Create a new nomination
    app.post('/events', Events.create);

    // Retrieve all nominations
    app.get('/events', Events.getAllEvents);

    // Retrieve a single nomination with nomination id
    app.get('/events/:eventId', Events.findEvent);

    // Update a nomination with nominationId
    app.put('/events/:eventId', Events.update);

    // Delete a Nomination with nominationId
    app.delete('/events/:eventId', Events.delete);
}

