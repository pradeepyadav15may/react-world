const Events = require('../models/events.model.js');

// Create and Save a new Nomination
exports.create = (req, res) => {
	// Validate request
    if(!req.body.name) {
        return res.status(400).send({
            message: "Name can not be empty"
        });
    }

    // Create a Nomination
    const event = new Events({
        name: req.body.name,
        venue : req.body.venue,
        detail : req.body.detail,
        date : req.body.date,
        attendies : req.body.attendies || []
    });

    // Save Nomination in the database
    event.save()
    .then(data => {
        //  res.send({message:"event Created Successfully!!!"});
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the User."
        });
    });
};

// Retrieve all Events from the database.
exports.getAllEvents = (req, res) => {
	Events.find()
    .then(events => {
        res.send(events);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving events."
        });
    });
};

// Find a nomination with a eventId
exports.findEvent = (req, res) => {
	Events.findById(req.params.eventId)
    .then(event => {
        if(!event) {
            return res.status(404).send({
                message: "Nomination not found with id " + req.params.eventId
            });            
        }
        let data = {
            attendies : event.attendies,
            name: event.name
        }

        res.send(data);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Nomination not found with id " + req.params.eventId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving nomination with id " + req.params.eventId
        });
    });
};

// Update nomination by the nominationId in the request
exports.update = (req, res) => {
	// Validate Request
    if(!req.body) {
        return res.status(400).send({
            message: "Something need to be there in request to update."
        });
    }

    // Find note and update it with the request body
    Events.findByIdAndUpdate(req.params.eventId, req.body, {new: true})
    .then(event => {
        if(!event) {
            return res.status(404).send({
                message: "event not found with id " + req.params.eventId
            });
        }
        res.send(event);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "event not found with id " + req.params.eventId
            });                
        }
        return res.status(500).send({
            message: "Error updating event with id " + req.params.eventId
        });
    });
};

// Delete Individual nominee nomination with the nominationId in the request
exports.delete = (req, res) => {
	Event.findByIdAndRemove(req.params.eventId)
    .then(event => {
        if(!event) {
            return res.status(404).send({
                message: "event not found with id " + req.params.eventId
            });
        }
        res.send({message: "event deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "event not found with id " + req.params.eventId
            });                
        }
        return res.status(500).send({
            message: "Could not delete event with id " + req.params.eventId
        });
    });
};