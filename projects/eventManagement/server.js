const express = require('express');
const bodyParser = require('body-parser');
// Configuring the database
const dbConfig = require('./config/db.config.js');
const mongoose = require('mongoose');
const cors = require('cors');

mongoose.Promise = global.Promise;

// Connecting to the database
mongoose.connect(dbConfig.mongoURI, {
    useNewUrlParser: true
    })
.then(() => {
    console.log("Successfully connected to the database");    
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...');
    process.exit();
});

// create express app
const app = express(),
  	  port = process.env.PORT || 5000;

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

// parse requests of content-type - application/json
app.use(bodyParser.json())
app.use(cors())

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
//default route
app.get('/', (req, res) => {
    res.json({"message": "Welcome to Nomination tool application. Manage and keep track of all your nominations."});
});

// Require User routes
require('./api/routes/user.routes.js')(app);
// Require Nomination routes
require('./api/routes/events.routes.js')(app);

app.listen(port);

console.log('nomination tool RESTful API server started on: ' + port);
