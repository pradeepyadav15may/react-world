﻿const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Address = new Schema({
        city: String
});

const employeeSchema = new Schema({
        name: String,
        address: String,
        age: Number,
        title: String,
        createdAt: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Employee', employeeSchema);


// why schema - to validate the data before saving it to the DB.