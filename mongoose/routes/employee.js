﻿const express = require('express');
const router = express.Router();
const Employee = require('../models/employee-model');

router.get('/', function (req, res, next) {
		Employee.find({address:'GGN'},(err, result) => {
						if (err) {
										next(err);
						}
						res.send(result);
		})
});

router.post('/', function (req, res, next) {
		const e1 = new Employee(req.body);
		e1.save((err, result) => {
						res.send('Employee saved');

		});
		
});

router.delete('/', function(req, res, next) {
		Employee.deleteOne({name: req.name}, (err) => {
						if(err) {
										next(err);
						}
						res.send('Deleted');
		});
});

router.put('/', function (req, res, next) {
		Employee.updateOne(
						req.body.filter, 
						req.body.emp, 
						(err) => {
						if(err) {
										next(err);
						}
						res.send('Updated');
		});
});
module.exports = router;
