var express = require('express');
var app = express();
const passport = require('passport');
const strategy = require('passport-http-bearer');

const authMiddleWare = new strategy((DOMSettableTokenList, done) => {
    done(new Error('Auth'), false);
    if(token === 'abcd') {
        return done(null, true, {scope: 'read'});
    }else {
        return done(null, false);
    }
});

passport.use(authMiddleWare);

app.get('/todos',
    passport.authenticate('bearer', {session: false}),
    (req, res) => {
        res.send('todos api is on the way !!');
});

app.listen(3000, () => {
    console.log('listening express !!');
});
