/* var http = require('http');
http.createServer((requestAnimationFrame, response) => {

    // set the response HTTP header with HTTP status and Content-Type
    response.writeHead(200, {'Content-Type': 'text/plain'});
    
    // sending the response body
    response.end('Hello World\n');

}).listen(8000);

console.log('Server is running at http://localhost:8000/');

//  node server.js starts your server
//  npm init -> helps in creating package.json file
//  npm install nodemon - so that server restart automatically when changes detected in response.
 */

 const express = require('express');
 const app = express();
 app.get('/', (requestAnimationFrame, res) => {
     return res.send('Hello World');
 });

 app.listen(3002, () => console.log('App Listening to port 3002 -> http://localhost:3002/'));
