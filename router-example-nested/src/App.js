import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import {BrowserRouter as Router, Route, Link, Switch} from 'react-router-dom';
import HomeComponent from './components/home';
import AboutComponent from './components/about';
import HelloComponent from './components/hello';
import BooksComponent from './components/books';

class App extends Component {
  render() {
    return (
      <Router>
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <div className="container">
            <ul>
              <li><Link to="/" >Home</Link></li>
              <li><Link to="/hello">Hello</Link></li>
              <li><Link to="/about">About</Link></li>
              <li><Link to="/books">Books</Link></li>
            </ul>
            <hr />
          
              <Route path="/" exact={true} component={HomeComponent} />
              <Route path="/hello" component={HelloComponent} />
              <Route path="/about" component={AboutComponent} />
              <Route path="/books" component={BooksComponent} />
            
        </div>
      </div>
      </Router>
    );
  }
}

export default App;
