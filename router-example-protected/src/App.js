import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import {BrowserRouter as Router, Switch, Link, Redirect, Route} from 'react-router-dom';
import HomeComponent from './components/home';
import AboutComponent from './components/about';
import HelloComponent from './components/hello';
import LoginComponent from './components/login';
import AdminComponent from './components/admin';
import auth from './components/auth';
import {PrivateRoute} from './components/privateroute';

class App extends Component {
  render() {
    return (
      <Router>
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <div className="container">
          <ul>
            <li><Link to="/">Home</Link></li>
            <li><Link to="/hello">Hello</Link></li>
            <li><Link to="/about">About</Link></li>
            <li><Link to="/login">Login</Link></li>
            <li><Link to="/admin">Admin</Link></li>
          </ul>
          <hr />
          <Switch>
            <Route exact path="/" component={HomeComponent} />
            <Route path="/hello" component={HelloComponent} />
            <Route path="/about" component={AboutComponent} />
            <Route path="/login" component={LoginComponent} />
            <PrivateRoute authed={auth.isAuthenticated} path="/admin" component={AdminComponent} />
          </Switch>
        </div>
      </div>
      </Router>
    );
  }
}

export default App;
