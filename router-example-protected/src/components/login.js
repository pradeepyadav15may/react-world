import React, {Component} from 'react';
import auth from './auth';
import {Redirect} from 'react-router-dom';

class LoginComponent extends Component {
    constructor() {
      super();
      this.email='';
      this.password='';
      this.state = {
        redirectToReferrer: false
      }
      // binding 'this'
      this.login = this.login.bind(this);
    }
  
    login() {
  
      auth.authenticate(this.email.value, this.password.value,() => {
        this.setState({ redirectToReferrer: true })
      })
    }
  
    render() {
      const { from } = this.props.location.state || { from: { pathname: '/' } }
      const { redirectToReferrer } = this.state;
  
      if (auth.message==='' && redirectToReferrer) {
        console.log({from});
        console.log('{from}');
        return (
          <Redirect to={from} />
        )
      }
  
      return (
        <div className="jumbotron">
            <h1 className="display-3">Login required</h1>
            <p className="lead">You must log in to view the page at {from.pathname}.</p>
            <p className="lead">
              <label hidden={auth.message ===''} className="warning">{auth.message}</label>
              <br />
              <input type="email" ref={node => this.email=node} placeholder="email" />
              <input type="password" ref={node => this.password=node} placeholder="" />
              <a className="btn btn-primary btn-lg" onClick={this.login} role="button">Login</a>
            </p>
        </div>
      )
    }
  
}

export default LoginComponent;