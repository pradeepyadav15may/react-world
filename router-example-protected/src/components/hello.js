import React from 'react';

const HelloComponent = () => {
    return(
        <div className="card">
            <div className="card-header">
                <h3>Hello Component</h3>
            </div>
        </div>
    )
}

export default HelloComponent;