import React from 'react';
import {
    Route,
    Link,
    Switch
  } from 'react-router-dom';

const BooksComponent = ({ match }) => {
    return (
        <div>
            <div className="jumbotron">
                <h1 className="display-3">My Books</h1>
            </div>

            <div className="container">
            <div className="row">
                <div className="col-md-3">
                    <ul>
                    <li><Link to={`${match.url}/html`}>HTML</Link></li>
                    <li><Link to={`${match.url}/css`}>CSS</Link></li>
                    <li><Link to={`${match.url}/react`}>React</Link></li>
                    </ul>
                </div>
                <div className="col-md-9">
                  <Switch>
                    <Route path={`${match.path}/:id`} component={Child} />
                </Switch>
                </div>
            </div>
            </div>
        </div>
    );
}

const Child = ({ match }) => (
    <div>
      <h3>URL ID parameter: {match.params.id}</h3>
    </div>
);

export default BooksComponent;