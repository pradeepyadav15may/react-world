import React from 'react';

const HelloComponent = () => {
    return (
        <div className="card">
            <h3>Hello Component</h3>
        </div>
    )
}

export default HelloComponent;