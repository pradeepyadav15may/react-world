import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import {BrowserRouter as Router, Switch, Link, Route} from 'react-router-dom';
import HomeComponent from './components/home';
import HelloComponent from './components/hello';
import AboutComponent from './components/about';
import BooksComponent from './components/books';

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <h1 className="App-title">Welcome to React</h1>
          </header>
          <div className="container">
            <ul>
              <li><Link to="/">Home</Link></li>
              <li><Link to="/about">About</Link></li>
              <li><Link to="/hello">Hello</Link></li>
              <li><Link to="/books">Books</Link></li>
            </ul>
            <hr />
            <Switch>
              <Route exact path="/" component={HomeComponent} />
              <Route path="/about" component ={AboutComponent} />
              <Route path="/hello" component = {HelloComponent} />
              <Route path="/books" component = {BooksComponent} />
            </Switch>
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
