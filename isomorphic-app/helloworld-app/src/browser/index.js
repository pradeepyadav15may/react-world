import React from 'react'
import {hydrate} from 'react-dom';

import App from '../shared/App';

const user = {name: "sohan"};
hydrate(
    <App data={window.__INITIAL_DATA__}/>,
    document.getElementById('app')
)

// Advantage of using 'hybrate' - When server sends the HTML markup then as soon as it loads the bundle.js then 
// because of bundle.js it tries to recreate the markup again so to avoid recreation of markup use hydrate. 
