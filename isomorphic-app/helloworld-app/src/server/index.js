import express from 'express';
import cors from 'cors';
import {renderToString} from 'react-dom/server';
import React from 'react'
import serialize from 'serialize-javascript';

import App from '../shared/App';

const app = express();

app.use(cors());

app.use(express.static('public'));
const user = {name: "mohan kumar singh"};
app.get('*', (req, res) => {
    const markup = renderToString(<App user={user}/>);

    res.send(`
        <!DOCTYPE html>
        <html>
            <head>
                <title>Server Side Rendering</title>
                <script>
                    window.__INITIAL_DATA__=${serialize(user)}
                </script>
            </head>
            <body>
                <div id="app">${markup}</div>
            </body>
        </html>
    `);
});

app.listen(3400, () => {
    console.log('App listening on port 3400!');
});