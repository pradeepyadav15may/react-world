1. yarn add react react-dom express cors isomorphic-fetch react-router-dom serialize-javascript

2. yarn add --dev babel-core babel-loader babel-plugin-transform-object-rest-spread babel-preset-env babel-preset-react nodemon webpack webpack-node-externals webpack-cli

3 Add the following line to package.json
"babel":{
      "presets": [
        "env",
        "react"
      ],
      "plugins": [
        "transform-object-rest-spread"
      ]
  },

4. Add scripts section in package.json
"scripts": {
    "start": "webpack -w & nodemon server.js"
  },

5. Create webpack.config.js with following content
  5.1 Create browserConfig and serverConfig sections

6. Add following elements to browserConfig sections
7. Add following elements to serverConfig sections
8. Now create a following folder structure
  8.1 src/browser
  8.2 src/server
  8.3 src/shared
9. Now create following file in created folder
  9.1 src/browser/index.js
  9.2 src/server/index.js
  9.3 src/shared/App.js
10. Now add following content to App.js
  10.1 
11. Add the following code to server/index.js
  app.use(express.static('public'));

app.get('*', (req, res) => {
    const markup = renderToString(<App />);

    res.send(`
        <!DOCTYPE html>
        <html>
            <head>
                <title>Server Side Rendering</title>
            </head>
            <body>
                <div id="app">${markup}</div>
            </body>
        </html>


    `);
});


12. Run the app using yarn start
12.1 Go to the browser and see the network tab to make sure the app is rendered from the server

13.  Now go to the server/index.js and add the following line below the title
     <script src='/bundle.js' defer></script>

    13.1 But now we have to go and implement the browser/index.js
    13.2 Here instead of using render we are using hydrate 

    13.3 Since we are using server rendering what hydrate does it tells browser that instead of recreating markup just preserve the existing markup and attach the event handler which is required.


